package com.eslam.BellyFilly.services;

import com.eslam.BellyFilly.models.Order;
import com.eslam.BellyFilly.models.OrderDetail;
import com.eslam.BellyFilly.services.databaseServices.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;

    public void saveOrder(Order order) {
        orderRepository.save(order);
    }

    public void calculateOrderPrice(Order order) {
        int total = 0;
        if (order != null)
            for (OrderDetail orderDetail : order.getOrderItems()) {
                total += orderDetail.getItemCount() * orderDetail.getMenuItem().getPrice();
            }
        order.setTotalPrice(total);
            System.out.println(total);
    }
}
