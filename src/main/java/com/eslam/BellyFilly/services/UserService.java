package com.eslam.BellyFilly.services;

import com.eslam.BellyFilly.models.*;
import com.eslam.BellyFilly.security.PasswordEncryptionService;
import com.eslam.BellyFilly.services.databaseServices.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncryptionService passwordEncryptionService;
    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderHistoryService orderHistoryService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findById(s);
        user.orElseThrow(() -> new UsernameNotFoundException("User with email: " + s + "wasn't found"));
        UserDetailsImplementation userDetails = new UserDetailsImplementation(user.get());
        return userDetails;
    }

    public User getUser(String email) {
        Optional<User>  user =this.userRepository.findById(email);
        if(user.isPresent() ) {
            return user.get();
        }
        else throw new UsernameNotFoundException("User with email: " + email + "wasn't found");
    }

    private void saveUserDetails(User user) {
        if (user.getPassword() != null) {
            user.setEncryptedPassword(passwordEncryptionService.encryptPassword(user.getPassword()));
        } else {
           throw new AuthenticationErrorException("Password must be entered");
        }
        user.setActive(true);
        if (user.getRoles().contains("USER")) {
            this.userRepository.save(user);
            OrderHistory orderHistory = new OrderHistory();
            user.setOrderHistory(orderHistory);
            orderHistory.setUser(user);
        }

        this.userRepository.save(user);

    }

    public void addAdmin(User user) {
        if(!userRepository.findById(user.getEmail()).isPresent()) {
            user.setRoles("ADMIN");
            saveUserDetails(user);
        }
        else{
            throw new AuthenticationErrorException("Username already exists");
        }
    }

    public void addUser(User user) {
        if(!userRepository.findById(user.getEmail()).isPresent()) {
            user.setRoles("USER");
            saveUserDetails(user);
        }
        else{
            throw new AuthenticationErrorException("Username already exists");
        }
    }

    public void updateUser(User user) {
        user.setRoles("USER");
        this.userRepository.save(user);
    }

    public void newOrder(String email, Order order) {
        User user = getUser(email);
        order.setOrderDate(new Date());
        order.setUser(user);
        user.setCurrentOrder(order);
        user.getOrderHistory().getOrders().add(order);
        this.userRepository.save(user);
        orderService.calculateOrderPrice(user.getCurrentOrder());
        orderService.saveOrder(user.getCurrentOrder());

    }

    public int checkout(String email) {
        User user = getUser(email);
        int total =user.getCurrentOrder().getTotalPrice();
        user.setCurrentOrder(null); //handle this exception
        userRepository.save(user);
        return total;
    }

    public void deleteUser(String email) {
        userRepository.deleteById(email);
    }

    public static User copyUserInfo(User user) {
        User returnedUser = new User();
        returnedUser.setName(user.getName());
        returnedUser.setEmail(user.getEmail());
        returnedUser.setAddress(user.getAddress());
        returnedUser.setPhoneNumber(user.getPhoneNumber());
    return returnedUser;
    }

    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
}