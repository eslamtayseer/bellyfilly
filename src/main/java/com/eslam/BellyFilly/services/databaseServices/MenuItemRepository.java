package com.eslam.BellyFilly.services.databaseServices;

import com.eslam.BellyFilly.models.MenuItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuItemRepository extends JpaRepository<MenuItem,Integer> {
}
