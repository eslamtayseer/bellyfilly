package com.eslam.BellyFilly.services.databaseServices;

import com.eslam.BellyFilly.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,String> {
}
