package com.eslam.BellyFilly.services.databaseServices;

import com.eslam.BellyFilly.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order,Integer> {
}
