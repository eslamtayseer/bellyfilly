package com.eslam.BellyFilly.services.databaseServices;

import com.eslam.BellyFilly.models.OrderHistory;
import com.eslam.BellyFilly.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderHistoryRepository extends JpaRepository<OrderHistory,Integer> {
    public Optional<OrderHistory> findByUser(User user);
}
