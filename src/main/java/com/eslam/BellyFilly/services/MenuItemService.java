package com.eslam.BellyFilly.services;

import com.eslam.BellyFilly.models.MenuItem;
import com.eslam.BellyFilly.services.databaseServices.MenuItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class MenuItemService {
    @Autowired
    private MenuItemRepository  menuItemRepository;

    public List<MenuItem> getMenuItems(){
       return menuItemRepository.findAll();
    }

    public MenuItem getMenuItem (int id) {
        Optional<MenuItem> menuItemOptional= menuItemRepository.findById(id) ;
        menuItemOptional.orElseThrow(()->new ResponseStatusException(HttpStatus.NOT_FOUND));
        return menuItemOptional.get();
    }

    public void addMenuItem (MenuItem menuItem){
        this.menuItemRepository.save(menuItem);
    }
    public void updateMenuItem(int id,MenuItem menuItem){
        menuItem.setId(id);
        menuItemRepository.save(menuItem);
    }
    public void deleteMenuItem (int id){
        this.menuItemRepository.deleteById(id);
    }

}

