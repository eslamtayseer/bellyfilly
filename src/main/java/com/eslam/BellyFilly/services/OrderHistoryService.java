package com.eslam.BellyFilly.services;

import com.eslam.BellyFilly.models.OrderHistory;
import com.eslam.BellyFilly.models.User;
import com.eslam.BellyFilly.services.databaseServices.OrderHistoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class OrderHistoryService {

    @Autowired
    private OrderHistoryRepository orderHistoryRepository;

    public OrderHistory getOrderHistoryOfUser(User user){
        Optional<OrderHistory> orderHistory= this.orderHistoryRepository.findByUser(user); //handle null
        return orderHistory.get();
    }
    public void addOrderHistory(OrderHistory orderHistory){
        this.orderHistoryRepository.save(orderHistory);
    }

}
