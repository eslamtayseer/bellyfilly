package com.eslam.BellyFilly.controllers;

import com.eslam.BellyFilly.controllers.controllerInterfaces.UserControllerInterface;
import com.eslam.BellyFilly.models.Order;
import com.eslam.BellyFilly.models.OrderHistory;
import com.eslam.BellyFilly.models.User;
import com.eslam.BellyFilly.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController implements UserControllerInterface {
    @Autowired
    UserService userService;


    @Override
    public void updateUserInformation(String email,User user) {
        user.setEmail(email);
        this.userService.updateUser(user);
    }

    @Override
    public OrderHistory getOrderHistory(String email) {
        return userService.getUser(email).getOrderHistory();
    }

    @Override
    public void newOrder(String email, Order order) {
        System.out.println(email);
        userService.newOrder(email,order);
    }

    @Override
    public User getUserByEmail(String email) {
        return UserService.copyUserInfo( userService.getUser(email));
    }

    @Override
    public int checkout(String email) {
        return userService.checkout(email);
    }



}
