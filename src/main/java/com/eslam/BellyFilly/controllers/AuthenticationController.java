package com.eslam.BellyFilly.controllers;

import com.eslam.BellyFilly.controllers.controllerInterfaces.AuthenticationControllerInterface;
import com.eslam.BellyFilly.models.AuthenticationRequest;
import com.eslam.BellyFilly.models.AuthenticationResponse;
import com.eslam.BellyFilly.models.User;
import com.eslam.BellyFilly.security.InvalidJWTService;
import com.eslam.BellyFilly.security.JwtService;
import com.eslam.BellyFilly.security.PasswordEncryptionService;
import com.eslam.BellyFilly.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
public class AuthenticationController implements AuthenticationControllerInterface {

    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncryptionService passwordEncryptionService;
    @Autowired
    private InvalidJWTService invalidJWTService;
    Logger log = Logger.getAnonymousLogger();
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        final UserDetails userDetails = userService
                .loadUserByUsername(authenticationRequest.getEmail());
        String encryptedUserPassword = userDetails.getPassword();
        if(passwordEncryptionService.checkPassword(authenticationRequest.getPassword(),encryptedUserPassword)) {
            final String jwt = jwtService.generateToken(userDetails);
            log.info("User Login");
            return ResponseEntity.ok(new AuthenticationResponse(jwt));
        }
        else{
            log.log(Level.INFO,"Invalid LogIn information");
            throw new BadCredentialsException("Incorrect username or password");
        }
    }

    public ResponseEntity<?> signUpAndCreateAuthenticationToken(@RequestBody User user) throws Exception {
        userService.addUser(user);
            return ResponseEntity.ok(new AuthenticationResponse(getUserJwt(user.getEmail())));
    }


    public ResponseEntity<?> signUpAndCreateAuthenticationTokenForAdmin(@RequestBody User user) throws Exception {
        userService.addAdmin(user);
        return ResponseEntity.ok(new AuthenticationResponse(getUserJwt(user.getEmail())));
    }


    public void logOut(String jwt) {
        invalidJWTService.addJWT(jwt.substring(7));
    }

    private String getUserJwt(String email){
        final UserDetails userDetails = userService
                .loadUserByUsername(email);
        final String jwt = jwtService.generateToken(userDetails);
        return jwt;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setJwtService(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    public void setPasswordEncryptionService(PasswordEncryptionService passwordEncryptionService) {
        this.passwordEncryptionService = passwordEncryptionService;
    }

    public void setInvalidJWTService(InvalidJWTService invalidJWTService) {
        this.invalidJWTService = invalidJWTService;
    }
}
