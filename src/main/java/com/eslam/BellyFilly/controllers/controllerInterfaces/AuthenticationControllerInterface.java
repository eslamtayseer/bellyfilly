package com.eslam.BellyFilly.controllers.controllerInterfaces;

import com.eslam.BellyFilly.models.AuthenticationRequest;
import com.eslam.BellyFilly.models.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


public interface AuthenticationControllerInterface {

    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception;
    @PostMapping("/signup")
    public ResponseEntity<?> signUpAndCreateAuthenticationToken(@RequestBody User user) throws Exception ;
    @PostMapping("/admin-signup")
    public ResponseEntity<?> signUpAndCreateAuthenticationTokenForAdmin(@RequestBody User user) throws Exception ;

    @GetMapping("/signout")
    public void logOut(@RequestHeader("authorization") String jwt);
    }