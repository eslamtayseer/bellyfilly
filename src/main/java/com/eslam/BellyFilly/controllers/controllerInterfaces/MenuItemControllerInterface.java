package com.eslam.BellyFilly.controllers.controllerInterfaces;

import com.eslam.BellyFilly.models.MenuItem;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/menu-items")
public interface MenuItemControllerInterface {

    @RequestMapping("")
    public List<MenuItem> getMenuItems();

    @RequestMapping("/{id}")
    public MenuItem getMenuItem(@PathVariable int id);

    @PostMapping("")
    public void AddMenuItem(@RequestBody MenuItem menuItem);

    @DeleteMapping("/{id}")
    public void deleteMenuItem(@PathVariable int id);

    @PutMapping("/{id}")
    public void updateMenuItem(@PathVariable int id,@RequestBody MenuItem menuItem);

}
