package com.eslam.BellyFilly.controllers.controllerInterfaces;

import com.eslam.BellyFilly.models.Order;
import com.eslam.BellyFilly.models.OrderHistory;
import com.eslam.BellyFilly.models.User;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/users")
public interface UserControllerInterface {

    @PostMapping("/{email}")
    public void updateUserInformation(@PathVariable("email")String email, @RequestBody User user);

    @RequestMapping("/{email}/orderHistory")
    public OrderHistory getOrderHistory(@PathVariable("email") String email);

    @PostMapping ("/{email}/order")
    public void newOrder(@PathVariable("email") String email, @RequestBody Order order);

    @RequestMapping("/{email}")
    public User getUserByEmail(@PathVariable("email") String email);

    @RequestMapping("/{email}/checkout")
    public int checkout(@PathVariable("email") String email);


}
