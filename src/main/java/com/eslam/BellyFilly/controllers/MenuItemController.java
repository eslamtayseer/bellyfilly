package com.eslam.BellyFilly.controllers;

import com.eslam.BellyFilly.controllers.controllerInterfaces.MenuItemControllerInterface;
import com.eslam.BellyFilly.models.MenuItem;
import com.eslam.BellyFilly.services.MenuItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class MenuItemController implements MenuItemControllerInterface {

    @Autowired
    MenuItemService menuItemService;


    @Override
    public List<MenuItem> getMenuItems() {
        return menuItemService.getMenuItems();
    }

    @Override
    public MenuItem getMenuItem(int id)  {

        return menuItemService.getMenuItem(id);
    }

    @Override
    public void AddMenuItem(MenuItem menuItem) {
    menuItemService.addMenuItem(menuItem);
    }

    @Override
    public void deleteMenuItem(int id) {
         menuItemService.deleteMenuItem(id);
    }

    @Override
    public void updateMenuItem(int id, MenuItem menuItem) {
        menuItemService.updateMenuItem(id,menuItem);
    }
}
