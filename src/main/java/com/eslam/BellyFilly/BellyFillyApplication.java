package com.eslam.BellyFilly;

import com.eslam.BellyFilly.models.User;
import com.eslam.BellyFilly.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BellyFillyApplication {
	@Autowired
	static UserService userService;
	public static void main(String[] args) {
		System.setProperty("server.servlet.context-path", "/bellyfilly");
		SpringApplication.run(BellyFillyApplication.class, args);

	}

}
