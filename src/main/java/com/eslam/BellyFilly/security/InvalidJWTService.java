package com.eslam.BellyFilly.security;

import com.eslam.BellyFilly.models.BlackListedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvalidJWTService {
    @Autowired
    JWTRepository jwtRepository;

    public boolean findJWT(String jwt){
       Optional<BlackListedJWT> blackListedJWT= jwtRepository.findByJwt(jwt);
       if(blackListedJWT.isPresent()) return true;
       return false;
    }

    public void addJWT (String jwt){
        BlackListedJWT blackListedJWT = new BlackListedJWT();
        blackListedJWT.setJwt(jwt);
        this.jwtRepository.save(blackListedJWT);
    }

    public void setJwtRepository(JWTRepository jwtRepository) {
        this.jwtRepository = jwtRepository;
    }
}
