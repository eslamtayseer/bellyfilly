package com.eslam.BellyFilly.security;

import com.eslam.BellyFilly.models.BlackListedJWT;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JWTRepository extends JpaRepository<BlackListedJWT,Integer> {
    public Optional<BlackListedJWT> findByJwt(String jwt);
}
