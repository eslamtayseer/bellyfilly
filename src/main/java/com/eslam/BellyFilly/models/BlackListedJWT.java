package com.eslam.BellyFilly.models;

import javax.persistence.*;

@Entity
public class BlackListedJWT {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    int id;

    @Column(length = 200)
    private String jwt;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }
}
