package com.eslam.BellyFilly.models;

import org.springframework.security.core.AuthenticationException;

public class AuthenticationErrorException extends AuthenticationException {
    public AuthenticationErrorException(String msg) {
        super(msg);
    }
}
