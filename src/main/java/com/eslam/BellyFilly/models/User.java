package com.eslam.BellyFilly.models;


import javax.persistence.*;

@Entity
public class User {

    @Id
    @Column(length=100)
    private String email;
    private String Name;

    @Transient
    private String password;

    @Column(name = "password",length = 100)
    private String encryptedPassword;

    private boolean active;
    private String roles;
    private String address;
    private String phoneNumber;

    @OneToOne(cascade = {CascadeType.ALL},orphanRemoval = true)
    private Order currentOrder;

    @OneToOne(cascade = {CascadeType.ALL},orphanRemoval = true)
    private OrderHistory orderHistory ;


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public OrderHistory getOrderHistory() {
        return orderHistory;
    }

    public void setOrderHistory(OrderHistory orderHistory) {
        this.orderHistory = orderHistory;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Order getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Order currentOrder) {
        this.currentOrder = currentOrder;
    }
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public void setEncryptedPassword(String encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }


}
