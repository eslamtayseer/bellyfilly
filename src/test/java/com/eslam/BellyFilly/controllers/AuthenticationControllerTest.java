package com.eslam.BellyFilly.controllers;

import com.eslam.BellyFilly.models.AuthenticationErrorException;
import com.eslam.BellyFilly.models.AuthenticationRequest;
import com.eslam.BellyFilly.models.AuthenticationResponse;
import com.eslam.BellyFilly.models.User;
import com.eslam.BellyFilly.security.InvalidJWTService;
import com.eslam.BellyFilly.security.JWTRepository;
import com.eslam.BellyFilly.security.JwtService;
import com.eslam.BellyFilly.security.PasswordEncryptionService;
import com.eslam.BellyFilly.services.UserService;
import com.eslam.BellyFilly.services.databaseServices.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@SpringBootTest
class AuthenticationControllerTest {

//    @TestConfiguration
//    static class UserServiceImplTestContextConfiguration {
//        @Bean
//        public UserService UserService() {
//            return new UserService();
//        }
//    }
//
//    @TestConfiguration
//    static class PasswordEncryptionImplTestContextConfiguration {
//        @Bean
//        public PasswordEncryptionService passwordEncryptionService() {
//            return new PasswordEncryptionService();
//        }
//    }


    AuthenticationController authenticationController;
    @Autowired
    private JwtService jwtService;
    @Autowired
    private UserService userService;
    @Autowired
    private InvalidJWTService invalidJWTService;

    @MockBean
    private PasswordEncryptionService passwordEncryptionService;
    @MockBean
    private UserRepository userRepository;
    @MockBean
    private JWTRepository jwtRepository;

    User user;
    AuthenticationRequest authenticationRequest;
    @BeforeEach
    void setUp() {
        userRepository = mock(UserRepository.class);
        jwtRepository = mock(JWTRepository.class);
        passwordEncryptionService = mock(PasswordEncryptionService.class);
        authenticationController = new AuthenticationController();
        userService.setUserRepository(userRepository);
        invalidJWTService.setJwtRepository(jwtRepository);
        authenticationController.setUserService(userService);
        authenticationController.setPasswordEncryptionService(passwordEncryptionService);
        authenticationController.setJwtService(jwtService);
        authenticationController.setInvalidJWTService(invalidJWTService);

        user = new User();
        user.setEmail("eslam@google.com");
        user.setRoles("USER");
        user.setPassword("password");
        authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setEmail("eslam@google.com");
        authenticationRequest.setPassword("password");
    }

    @Test
    public void testLoginValid() throws Exception {

        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));
        when(passwordEncryptionService.checkPassword(any(), any())).thenReturn(true);
        ResponseEntity responseEntity = authenticationController.createAuthenticationToken(authenticationRequest);
        AuthenticationResponse authenticationResponse=(AuthenticationResponse) responseEntity.getBody();
        verify(userRepository).findById("eslam@google.com");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(authenticationResponse.getJwt().length()>0);

    }

    @Test
    public void testLoginInvalidPassword() throws Exception {
        when(userRepository.findById(anyString())).thenReturn(Optional.of(user));
        when(passwordEncryptionService.checkPassword(any(), any())).thenReturn(false);
        assertThrows(BadCredentialsException.class,()->  authenticationController.createAuthenticationToken(authenticationRequest));
        verify(userRepository).findById("eslam@google.com");
    }

    @Test
    public void testLoginInvalidUserName() {
        when(userRepository.findById(anyString())).thenReturn(Optional.ofNullable(null));
        when(passwordEncryptionService.checkPassword(any(), any())).thenReturn(false);
        assertThrows(UsernameNotFoundException.class,()->  authenticationController.createAuthenticationToken(authenticationRequest));
        verify(userRepository).findById("eslam@google.com");
    }


    @Test
    public void testSignUpValid() throws Exception {
        when(userRepository.findById(any())).thenReturn(Optional.ofNullable(null)).thenReturn(Optional.of(user));
//        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        ResponseEntity responseEntity =    authenticationController.signUpAndCreateAuthenticationToken(user);
        AuthenticationResponse authenticationResponse=(AuthenticationResponse) responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(authenticationResponse.getJwt().length()>0);
        verify(userRepository,times(2)).save(user);
    }

    @Test
    public void testSignUpAlreadyExistingUser() {
        when(userRepository.findById(any())).thenReturn(Optional.of(user));
        assertThrows(AuthenticationErrorException.class,()->  authenticationController.signUpAndCreateAuthenticationToken(user));
        verify(userRepository).findById("eslam@google.com");


    }
    @Test
    public void testSignUpNoPasswordEntered() {
        user.setPassword(null);
        when(userRepository.findById(any())).thenReturn(Optional.ofNullable(null));
        assertThrows(AuthenticationErrorException.class,()->  authenticationController.signUpAndCreateAuthenticationToken(user));
        verify(userRepository).findById("eslam@google.com");
    }

    @Test
    public void testLogOut() {
        authenticationController.logOut("Bearer myJwtString");
        verify(jwtRepository).save(any());
    }
}